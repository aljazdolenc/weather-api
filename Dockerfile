FROM openjdk:17-jdk-slim

VOLUME /tmp
EXPOSE 8080
ENV ARSO_URL="https://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observation_si_latest.xml"
ENV ARSO_RESPONSE_CACHE_TTL="3600000"
ARG JAR_FILE=target/weather-api-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","/app.jar"]
