# Weather API
- accepts 2 optional queryParameters: lon and lat
- fetches current weather data for Slovenia from ARSO
- caches response for 1 hour
- if both lon and lat are provided, it returns the closest weather station data
otherwise it returns data for all weather stations

## Additional info
- AWS Elastic Beanstalk
- Java 17, Spring Boot v3.1.1
- ArsoResponseTTL, ArsoUrl can be configured trough environment variables

### Start Application on localhost
1. Run:
    ```bash
    mvn spring-boot:run -Dspring-boot.run.profiles=local
    ```
2. Test locally:
    - http://localhost:8080/current
    - http://localhost:8080/current?lon=30&lat=45

### Start Application on docker
1. Run:
    ```bash
    mvn clean package && \
    docker build -t weather-api . && \
    docker run -p 8080:8080 weather-api
    ```
2. Test locally:
    - http://localhost:8080/current
    - http://localhost:8080/current?lon=30&lat=45

### Deployed application url
- http://weatherapienv11-env.eba-eymfprt4.eu-central-1.elasticbeanstalk.com/current
