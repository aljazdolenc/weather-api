#!/bin/bash

mvn clean package && \
docker build -t weather-api . && \
docker run -p 8080:8080 weather-api
