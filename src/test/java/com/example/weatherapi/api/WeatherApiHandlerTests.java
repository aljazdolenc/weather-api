package com.example.weatherapi.api;

import com.example.weatherapi.fakers.WeatherStationsFaker;
import com.example.weatherapi.generated.model.GetCurrent200Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;
import com.example.weatherapi.service.WeatherService;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WeatherApiHandlerTests {

    private WeatherApiHandler weatherApiHandler;

    @Mock
    private WeatherService weatherService;

    @BeforeEach
    void init() {
        weatherApiHandler = new WeatherApiHandler(weatherService);
    }

    @ParameterizedTest
    @CsvSource({"200, 50", "-200, 50", "50, 200", "50, -200"})
    void returns_400_error_if_coordinates_are_invalid(BigDecimal lat, BigDecimal lon) {
        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> weatherApiHandler.getCurrent(lat, lon)
        );

        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatusCode());
    }

    @Test
    void returns_500_error_if_weather_service_throws_exception() {
        when(weatherService.getCurrent(any(), any())).thenThrow(new RuntimeException());

        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> weatherApiHandler.getCurrent(BigDecimal.valueOf(50), BigDecimal.valueOf(50))
        );

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getStatusCode());
    }

    @Test
    void returns_200_with_stations_returned_from_weather_service() {
        final var weatherStationInfoList = WeatherStationsFaker.getWeatherStationInfoList();
        final var getCurrent200Response = new GetCurrent200Response().stations(weatherStationInfoList);
        when(weatherService.getCurrent(any(), any())).thenReturn(Mono.just(weatherStationInfoList));

        final var result = weatherApiHandler.getCurrent(BigDecimal.valueOf(50), BigDecimal.valueOf(50));

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(getCurrent200Response, result.getBody());
    }

}
