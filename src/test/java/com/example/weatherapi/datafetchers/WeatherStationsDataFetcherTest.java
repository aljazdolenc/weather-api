package com.example.weatherapi.datafetchers;

import com.example.weatherapi.service.EnvironmentService;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.test.StepVerifier;

import java.math.BigDecimal;

@ExtendWith(MockitoExtension.class)
class WeatherStationsDataFetcherTest {

    private WeatherStationsDataFetcher weatherStationsDataFetcherTest;

    private static MockWebServer server;

    @Mock
    private static EnvironmentService environmentService;

    @BeforeEach
    public void setup() {
        server = new MockWebServer();

        environmentService.arsoUrl = server.url("/").toString();

        weatherStationsDataFetcherTest = new WeatherStationsDataFetcher(environmentService);
    }

    @AfterEach
    void tearDown() throws Exception {
        server.shutdown();
    }

    @Test
    void throws_exception_when_response_fails() {
        server.enqueue(new MockResponse()
                .setResponseCode(500)
                .addHeader("Content-Type", "text/html; charset=utf-8")
                .addHeader("Cache-Control", "no-cache")
        );

        StepVerifier.create(weatherStationsDataFetcherTest.getWeatherStations())
                .expectErrorMatches(throwable -> throwable instanceof RuntimeException &&
                        throwable.getMessage().contains("MeteoArso request failed"))
                .verify();
    }

    @Test
    void throws_exception_when_parsed_weather_stations_list_is_empty() {
        server.enqueue(new MockResponse()
                .addHeader("Content-Type", "text/html; charset=utf-8")
                .addHeader("Cache-Control", "no-cache")
                .setBody("""
                        <?xml version="1.0" encoding="UTF-8"?>
                            <data id="MeteoSI_WebMet_observation_xml">
                        </data>
                        """)
        );

        StepVerifier.create(weatherStationsDataFetcherTest.getWeatherStations())
                .expectErrorMatches(throwable -> throwable instanceof RuntimeException &&
                        throwable.getMessage().contains("No weather stations found in MeteoArso response"))
                .verify();
    }

    @Test
    void returns_weather_stations_list() {
        server.enqueue(new MockResponse()
                .addHeader("Content-Type", "text/html; charset=utf-8")
                .addHeader("Cache-Control", "no-cache")
                .setBody("""
                        <?xml version="1.0" encoding="UTF-8"?>
                        <data id="MeteoSI_WebMet_observation_xml">
                            <metData>
                                <domain_lat>46.0933</domain_lat>
                                <domain_lon>14.3756</domain_lon>
                                <domain_title>KATARINA</domain_title>
                                <t>18</t>
                                <t_degreesC>18</t_degreesC>
                                <rh>24</rh>
                             </metData>
                        </data>
                        """)
        );

        StepVerifier.create(weatherStationsDataFetcherTest.getWeatherStations())
                .expectNextMatches(stations -> {
                    if(stations.size() != 1) {
                        return false;
                    }

                    final var station = stations.get(0).toWeatherStationInfo(null);

                    return station.getLat().equals(new BigDecimal("46.0933")) &&
                            station.getLon().equals(new BigDecimal("14.3756")) &&
                            station.getName().equals("KATARINA") &&
                            station.getTemperature().equals(new BigDecimal("18")) &&
                            station.getHumidity().equals(new BigDecimal("24")) &&
                            station.getDistance() == null;
                })
                .verifyComplete();
    }

}
