package com.example.weatherapi.datafetchers.data;

import static com.example.weatherapi.utilities.MockUtils.setField;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.example.weatherapi.datafetchers.data.WeatherStation;
import com.example.weatherapi.generated.model.WeatherStationInfo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

@ExtendWith(MockitoExtension.class)
class WeatherStationTest {

    @Test
    void maps_to_weather_station_info() {
        WeatherStation weatherStation = new WeatherStation();
        final double distance = 100.0;

        setField(weatherStation, "latitude", BigDecimal.valueOf(42.34));
        setField(weatherStation, "longitude", BigDecimal.valueOf(20.56));
        setField(weatherStation, "name", "Test Station");
        setField(weatherStation, "temperature", BigDecimal.valueOf(20));
        setField(weatherStation, "humidity", BigDecimal.valueOf(60));

        WeatherStationInfo info = weatherStation.toWeatherStationInfo(distance);

        assertEquals(BigDecimal.valueOf(42.34), info.getLat());
        assertEquals(BigDecimal.valueOf(20.56), info.getLon());
        assertEquals("Test Station", info.getName());
        assertEquals(BigDecimal.valueOf(20), info.getTemperature());
        assertEquals(BigDecimal.valueOf(60), info.getHumidity());
        assertEquals(BigDecimal.valueOf(distance), info.getDistance());
    }

}

