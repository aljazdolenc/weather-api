package com.example.weatherapi.fakers;

import com.example.weatherapi.datafetchers.data.WeatherStation;
import com.example.weatherapi.generated.model.WeatherStationInfo;
import org.openapitools.jackson.nullable.JsonNullable;

import java.math.BigDecimal;
import java.util.List;

import static com.example.weatherapi.utilities.MockUtils.setField;

public class WeatherStationsFaker {

    public static List<WeatherStationInfo> getWeatherStationInfoList() {
        final WeatherStationInfo weatherStationInfo = new WeatherStationInfo();
        weatherStationInfo.setLat(BigDecimal.valueOf(1));
        weatherStationInfo.setLon(BigDecimal.valueOf(1));
        weatherStationInfo.setName("weatherStationInfo1");
        weatherStationInfo.setDistance(BigDecimal.valueOf(1));
        weatherStationInfo.setTemperature(BigDecimal.valueOf(1));
        weatherStationInfo.setHumidity(BigDecimal.valueOf(1));

        final WeatherStationInfo weatherStationInfo2 = new WeatherStationInfo();
        weatherStationInfo2.setLat(BigDecimal.valueOf(2));
        weatherStationInfo2.setLon(BigDecimal.valueOf(2));
        weatherStationInfo2.setName("weatherStationInfo2");
        weatherStationInfo2.setDistance(BigDecimal.valueOf(2));
        weatherStationInfo2.setTemperature(BigDecimal.valueOf(2));
        weatherStationInfo2.setHumidity(BigDecimal.valueOf(2));

        return List.of(weatherStationInfo, weatherStationInfo2);
    }

    public static List<WeatherStation> getWeatherStationList() {
        final WeatherStation weatherStation = new WeatherStation();
        setField(weatherStation, "latitude", BigDecimal.valueOf(42.34));
        setField(weatherStation, "longitude", BigDecimal.valueOf(20.56));
        setField(weatherStation, "name", "weatherStation1");
        setField(weatherStation, "temperature", BigDecimal.valueOf(1));
        setField(weatherStation, "humidity", BigDecimal.valueOf(1));

        final WeatherStation weatherStation2 = new WeatherStation();
        setField(weatherStation2, "latitude", BigDecimal.valueOf(42.34));
        setField(weatherStation2, "longitude", BigDecimal.valueOf(20.56));
        setField(weatherStation2, "name", "weatherStation2");
        setField(weatherStation2, "temperature", BigDecimal.valueOf(2));
        setField(weatherStation2, "humidity", BigDecimal.valueOf(2));

        return List.of(weatherStation, weatherStation2);
    }

    public static List<WeatherStation> getWeatherStationListFromGeoLocations(List<GeoLocation> geoLocations) {
        return geoLocations.stream()
                .map(geoLocation -> {
                    final WeatherStation weatherStation = new WeatherStation();
                    setField(weatherStation, "latitude", geoLocation.latitude);
                    setField(weatherStation, "longitude", geoLocation.longitude);
                    setField(weatherStation, "name", "testWeatherStation");
                    setField(weatherStation, "temperature", BigDecimal.valueOf(1));
                    setField(weatherStation, "humidity", BigDecimal.valueOf(1));
                    return weatherStation;
                })
                .toList();
    }
}
