package com.example.weatherapi.fakers;

import java.math.BigDecimal;

public class GeoLocation {
    public final BigDecimal latitude;
    public final BigDecimal longitude;

    public GeoLocation(int latitude, int longitude) {
        this.latitude = BigDecimal.valueOf(latitude);
        this.longitude = BigDecimal.valueOf(longitude);
    }

}
