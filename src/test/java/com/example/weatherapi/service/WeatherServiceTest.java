package com.example.weatherapi.service;

import com.example.weatherapi.fakers.GeoLocation;
import com.example.weatherapi.fakers.WeatherStationsFaker;
import com.example.weatherapi.datafetchers.data.WeatherStation;
import com.example.weatherapi.datafetchers.WeatherStationsDataFetcher;
import com.example.weatherapi.generated.model.WeatherStationInfo;
import com.example.weatherapi.service.WeatherService;
import com.example.weatherapi.service.EnvironmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.math.BigDecimal;
import java.util.List;

import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class WeatherServiceTest {

    private WeatherService weatherService;
    private final List<WeatherStation> fakeWeatherStations = WeatherStationsFaker.getWeatherStationList();

    @Mock
    private WeatherStationsDataFetcher weatherStationsDataFetcher;

    @Mock
    private EnvironmentService environmentService;

    @BeforeEach
    void setUp() {
        weatherService = new WeatherService(weatherStationsDataFetcher, environmentService);
        environmentService.arsoResponseCacheTtl = 1000;
        weatherService.init();
    }

    @Test
    void returns_all_stations_with_distance_null_when_latitude_is_not_provided() {
        given(weatherStationsDataFetcher.getWeatherStations()).willReturn(Mono.just(this.fakeWeatherStations));
        final Mono<List<WeatherStationInfo>> weatherStations = weatherService.getCurrent(null, BigDecimal.ZERO);

        StepVerifier.create(weatherStations)
                .expectNextMatches(stations ->
                        stations.stream().allMatch(station -> station.getDistance() == null) && stations.size() == this.fakeWeatherStations.size()
                )
                .verifyComplete();
    }

    @Test
    void returns_all_stations_with_distance_null_when_longitude_is_not_provided() {
        given(weatherStationsDataFetcher.getWeatherStations()).willReturn(Mono.just(this.fakeWeatherStations));
        final Mono<List<WeatherStationInfo>> weatherStations = weatherService.getCurrent(BigDecimal.ZERO, null);

        StepVerifier.create(weatherStations)
                .expectNextMatches(stations ->
                        stations.stream().allMatch(station -> station.getDistance() == null) && stations.size() == this.fakeWeatherStations.size()
                )
                .verifyComplete();
    }

    @Test
    void returns_nearest_station_with_distance_when_latitude_and_longitude_are_provided() {
        final BigDecimal latitude = BigDecimal.valueOf(45);
        final BigDecimal longitude = BigDecimal.valueOf(45);
        final GeoLocation nearestStationLocation = new GeoLocation(46, 46);
        final GeoLocation farthestStationLocation = new GeoLocation(50, 50);
        final List<WeatherStation> fakeWeatherStations = WeatherStationsFaker.getWeatherStationListFromGeoLocations(
                List.of(nearestStationLocation, farthestStationLocation)
        );

        given(weatherStationsDataFetcher.getWeatherStations()).willReturn(Mono.just(fakeWeatherStations));

        final Mono<List<WeatherStationInfo>> weatherStations = weatherService.getCurrent(latitude, longitude);

        StepVerifier.create(weatherStations)
                .expectNextMatches(stations -> {
                            final WeatherStationInfo nearestStation = stations.get(0);

                            return nearestStation.getLat().equals(nearestStationLocation.latitude)
                                    && nearestStation.getLon().equals(nearestStationLocation.longitude)
                                    && stations.size() == 1;
                        }
                )
                .verifyComplete();
    }

}
