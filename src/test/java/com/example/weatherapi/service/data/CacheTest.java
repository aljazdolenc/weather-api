package com.example.weatherapi.service.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.lang.Thread.sleep;
import static org.junit.jupiter.api.Assertions.*;

class CacheTest {
    private Cache<String> cache;
    private static final int TTL = 1000;

    @BeforeEach
    void setUp() {
        cache = new Cache<>(TTL);
    }

    @Test
    void caches_value() {
        final var cachedValue = "Test";

        cache.cacheValue(cachedValue);

        assertEquals(cachedValue, cache.getCachedValue());
    }

    @Test
    void returns_null_if_ttl_of_cached_value_has_expired() throws InterruptedException {
        final var expiryTime = TTL + 500;
        cache.cacheValue("Test");

        sleep(expiryTime);

        assertNull(cache.getCachedValue());
    }

    @Test
    void can_update_cached_value() {
        cache.cacheValue("Test");
        cache.cacheValue("Test 2");

        assertEquals("Test 2", cache.getCachedValue());
    }
}
