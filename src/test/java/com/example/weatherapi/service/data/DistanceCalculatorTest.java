package com.example.weatherapi.service.data;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import com.example.weatherapi.service.data.DistanceCalculator;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class DistanceCalculatorTest {

    @Test
    void calculates_distance_between_two_points() {
        final BigDecimal stationLatitude = BigDecimal.valueOf(20);
        final BigDecimal stationLongitude = BigDecimal.valueOf(20);
        final BigDecimal targetLatitude = BigDecimal.valueOf(30);
        final BigDecimal targetLongitude = BigDecimal.valueOf(30);
        final int expectedDistance = 1499000;
        final double maximum1percentError = expectedDistance * 0.01;

        final double distance = DistanceCalculator.calculateDistance(
                stationLatitude,
                stationLongitude,
                targetLatitude,
                targetLongitude
        );

        assertEquals(1499000, distance, maximum1percentError);
    }

}
