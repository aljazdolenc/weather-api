package com.example.weatherapi.generated.model;

import java.net.URI;
import java.util.Objects;
import com.example.weatherapi.generated.model.WeatherStationInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * GetCurrent200Response
 */

@JsonTypeName("getCurrent_200_response")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class GetCurrent200Response {

  @JsonProperty("stations")
  @Valid
  private List<WeatherStationInfo> stations = null;

  public GetCurrent200Response stations(List<WeatherStationInfo> stations) {
    this.stations = stations;
    return this;
  }

  public GetCurrent200Response addStationsItem(WeatherStationInfo stationsItem) {
    if (this.stations == null) {
      this.stations = new ArrayList<>();
    }
    this.stations.add(stationsItem);
    return this;
  }

  /**
   * Get stations
   * @return stations
  */
  @Valid 
  @Schema(name = "stations", required = false)
  public List<WeatherStationInfo> getStations() {
    return stations;
  }

  public void setStations(List<WeatherStationInfo> stations) {
    this.stations = stations;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetCurrent200Response getCurrent200Response = (GetCurrent200Response) o;
    return Objects.equals(this.stations, getCurrent200Response.stations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(stations);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetCurrent200Response {\n");
    sb.append("    stations: ").append(toIndentedString(stations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

