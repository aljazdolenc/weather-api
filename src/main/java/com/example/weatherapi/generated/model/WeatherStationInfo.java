package com.example.weatherapi.generated.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.math.BigDecimal;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * WeatherStationInfo
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class WeatherStationInfo {

  @JsonProperty("lat")
  private BigDecimal lat;

  @JsonProperty("lon")
  private BigDecimal lon;

  @JsonProperty("name")
  private String name;

  @JsonProperty("distance")
  private BigDecimal distance;

  @JsonProperty("temperature")
  private BigDecimal temperature;

  @JsonProperty("humidity")
  private BigDecimal humidity;

  public WeatherStationInfo lat(BigDecimal lat) {
    this.lat = lat;
    return this;
  }

  /**
   * Latitude of the weather station from where data is taken. Mapping: `metData.domain_lat` 
   * @return lat
  */
  @Valid 
  @Schema(name = "lat", example = "45.8958", description = "Latitude of the weather station from where data is taken. Mapping: `metData.domain_lat` ", required = false)
  public BigDecimal getLat() {
    return lat;
  }

  public void setLat(BigDecimal lat) {
    this.lat = lat;
  }

  public WeatherStationInfo lon(BigDecimal lon) {
    this.lon = lon;
    return this;
  }

  /**
   * Longitude of the weather station from where data is taken. Mapping: `metData.domain_lon` 
   * @return lon
  */
  @Valid 
  @Schema(name = "lon", example = "13.6289", description = "Longitude of the weather station from where data is taken. Mapping: `metData.domain_lon` ", required = false)
  public BigDecimal getLon() {
    return lon;
  }

  public void setLon(BigDecimal lon) {
    this.lon = lon;
  }

  public WeatherStationInfo name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name of the weather station from where data is taken. Mapping: `metData.domain_title` 
   * @return name
  */
  
  @Schema(name = "name", example = "NOVA GORICA", description = "Name of the weather station from where data is taken. Mapping: `metData.domain_title` ", required = false)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public WeatherStationInfo distance(BigDecimal distance) {
    this.distance = distance;
    return this;
  }

  /**
   * Distance from current location and weather station (in meters) Returns -1 if `lat` AND|OR `lon` is not provided; 
   * @return distance
  */
  @Valid 
  @Schema(name = "distance", example = "123", description = "Distance from current location and weather station (in meters) Returns -1 if `lat` AND|OR `lon` is not provided; ", required = false)
  public BigDecimal getDistance() {
    return distance;
  }

  public void setDistance(BigDecimal distance) {
    this.distance = distance;
  }

  public WeatherStationInfo temperature(BigDecimal temperature) {
    this.temperature = temperature;
    return this;
  }

  /**
   * Current temperature Mapping: `metData.t` 
   * @return temperature
  */
  @Valid 
  @Schema(name = "temperature", example = "4", description = "Current temperature Mapping: `metData.t` ", required = false)
  public BigDecimal getTemperature() {
    return temperature;
  }

  public void setTemperature(BigDecimal temperature) {
    this.temperature = temperature;
  }

  public WeatherStationInfo humidity(BigDecimal humidity) {
    this.humidity = humidity;
    return this;
  }

  /**
   * Current humidity Mapping: `metData.rh` 
   * @return humidity
  */
  @Valid 
  @Schema(name = "humidity", example = "66", description = "Current humidity Mapping: `metData.rh` ", required = false)
  public BigDecimal getHumidity() {
    return humidity;
  }

  public void setHumidity(BigDecimal humidity) {
    this.humidity = humidity;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WeatherStationInfo weatherStationInfo = (WeatherStationInfo) o;
    return Objects.equals(this.lat, weatherStationInfo.lat) &&
        Objects.equals(this.lon, weatherStationInfo.lon) &&
        Objects.equals(this.name, weatherStationInfo.name) &&
        Objects.equals(this.distance, weatherStationInfo.distance) &&
        Objects.equals(this.temperature, weatherStationInfo.temperature) &&
        Objects.equals(this.humidity, weatherStationInfo.humidity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lat, lon, name, distance, temperature, humidity);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WeatherStationInfo {\n");
    sb.append("    lat: ").append(toIndentedString(lat)).append("\n");
    sb.append("    lon: ").append(toIndentedString(lon)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    distance: ").append(toIndentedString(distance)).append("\n");
    sb.append("    temperature: ").append(toIndentedString(temperature)).append("\n");
    sb.append("    humidity: ").append(toIndentedString(humidity)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

