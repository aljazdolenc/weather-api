package com.example.weatherapi.generated.api;

import java.math.BigDecimal;
import com.example.weatherapi.generated.model.GetCurrent200Response;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Generated;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
@Controller
public class CurrentApiController implements CurrentApi {

    private final CurrentApiDelegate delegate;

    public CurrentApiController(@Autowired(required = false) CurrentApiDelegate delegate) {
        this.delegate = Optional.ofNullable(delegate).orElse(new CurrentApiDelegate() {});
    }

    @Override
    public CurrentApiDelegate getDelegate() {
        return delegate;
    }

}
