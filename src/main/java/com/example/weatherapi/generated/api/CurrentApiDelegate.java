package com.example.weatherapi.generated.api;

import java.math.BigDecimal;
import com.example.weatherapi.generated.model.GetCurrent200Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Generated;

/**
 * A delegate to be called by the {@link CurrentApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public interface CurrentApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * GET /current : Get current weather
     * Get current weather based on specific location. * for datasource use: &#x60;https://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observation_si_latest.xml&#x60; * if &#x60;lat&#x60; AND &#x60;lon&#x60; are provided &gt; find nearest weather station and get data from it * if &#x60;lat&#x60; AND|OR &#x60;lon&#x60; is not provided &gt; return list off all stations (&#x60;distance&#x3D;null&#x60;) 
     *
     * @param lat Latitude of the current location (optional)
     * @param lon Longitude of the current location (optional)
     * @return Successful response (status code 200)
     *         or Bad request (status code 400)
     *         or Server error (status code 500)
     * @see CurrentApi#getCurrent
     */
    default ResponseEntity<GetCurrent200Response> getCurrent(BigDecimal lat,
        BigDecimal lon) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"stations\" : [ { \"distance\" : 123, \"name\" : \"NOVA GORICA\", \"temperature\" : 4, \"humidity\" : 66, \"lon\" : 13.6289, \"lat\" : 45.8958 }, { \"distance\" : 123, \"name\" : \"NOVA GORICA\", \"temperature\" : 4, \"humidity\" : 66, \"lon\" : 13.6289, \"lat\" : 45.8958 } ] }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
