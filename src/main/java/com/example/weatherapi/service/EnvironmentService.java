package com.example.weatherapi.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EnvironmentService {

    @Value("${arso.url}")
    public String arsoUrl;


    @Value("${arso.response.cache.ttl}")
    public int arsoResponseCacheTtl;

}
