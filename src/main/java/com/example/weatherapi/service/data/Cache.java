package com.example.weatherapi.service.data;

import java.util.Date;

public class Cache<T> {
    private final int timeToLive;
    private Date timestamp;
    private T cachedValue;

    public Cache(final int timeToLive) {
        this.timeToLive = timeToLive;
        this.timestamp = new Date();
    }

    public void cacheValue(final T value) {
        this.cachedValue = value;
        this.timestamp = new Date();
    }

    public T getCachedValue() {
        if (new Date().getTime() - this.timestamp.getTime() > this.timeToLive) {
            this.cachedValue = null;
        }

        return this.cachedValue;
    }

}
