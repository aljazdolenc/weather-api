package com.example.weatherapi.service.data;

import java.math.BigDecimal;

public class DistanceCalculator {

    private static final double EARTH_RADIUS = 6371e3; // metres

    public static double calculateDistance(
            BigDecimal stationLatitude,
            BigDecimal stationLongitude,
            BigDecimal locationLatitude,
            BigDecimal locationLongitude
    ) {
        final double radStationLat = Math.toRadians(stationLatitude.doubleValue());
        final double radLocationLat = Math.toRadians(locationLatitude.doubleValue());
        final double deltaLatitude = Math.toRadians(locationLatitude.subtract(stationLatitude).doubleValue());
        final double deltaLongitude = Math.toRadians(locationLongitude.subtract(stationLongitude).doubleValue());

        final double halfChordSquare = halfChordSquareFrom(deltaLatitude, deltaLongitude, radStationLat, radLocationLat);

        return EARTH_RADIUS * angularDistanceFrom(halfChordSquare);
    }

    private static double halfChordSquareFrom(
            final double deltaLatitude,
            final double deltaLongitude,
            final double radStationLat,
            final double radLocationLat
    ) {
        return Math.sin(deltaLatitude / 2) * Math.sin(deltaLatitude / 2)
                + Math.cos(radStationLat) * Math.cos(radLocationLat)
                * Math.sin(deltaLongitude / 2) * Math.sin(deltaLongitude / 2);
    }

    private static double angularDistanceFrom(double halfChordSquare) {
        return 2 * Math.atan2(Math.sqrt(halfChordSquare), Math.sqrt(1 - halfChordSquare));
    }

}
