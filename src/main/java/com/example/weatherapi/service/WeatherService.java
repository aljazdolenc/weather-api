package com.example.weatherapi.service;

import com.example.weatherapi.datafetchers.WeatherStationsDataFetcher;
import com.example.weatherapi.datafetchers.data.WeatherStation;
import com.example.weatherapi.generated.model.WeatherStationInfo;
import com.example.weatherapi.service.data.Cache;
import com.example.weatherapi.service.data.DistanceCalculator;
import com.example.weatherapi.service.EnvironmentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class WeatherService {

    private final WeatherStationsDataFetcher weatherStationsDataFetcher;

    private final EnvironmentService environmentService;

    private Cache<List<WeatherStation>> weatherStationsCache;

    @PostConstruct
    public void init() {
        log.info("Initializing WeatherStationsCache with TTL {} seconds", environmentService.arsoResponseCacheTtl);
        this.weatherStationsCache = new Cache<>(environmentService.arsoResponseCacheTtl);
    }

    public Mono<List<WeatherStationInfo>> getCurrent(
            final BigDecimal latitude,
            final BigDecimal longitude
    ) {
        return latitude == null || longitude == null
                ? getAllWeatherStations()
                : getNearestWeatherStation(latitude, longitude);
    }

    private Mono<List<WeatherStationInfo>> getAllWeatherStations() {
        return this.fetchAllWeatherStations()
                .map(stations -> stations
                        .stream()
                        .map(station -> station.toWeatherStationInfo(null))
                        .toList()
                );
    }

    private Mono<List<WeatherStationInfo>> getNearestWeatherStation(
            final BigDecimal latitude,
            final BigDecimal longitude
    ) {
        return this.fetchAllWeatherStations()
                .map(stations -> stations.stream()
                        .min(Comparator.comparingDouble(station -> DistanceCalculator.calculateDistance(latitude, longitude, station.getLat(), station.getLon())))
                        .map(station -> station.toWeatherStationInfo(DistanceCalculator.calculateDistance(latitude, longitude, station.getLat(), station.getLon())))
                        .map(Arrays::asList)
                        .orElseGet(ArrayList::new)
                );
    }

    private Mono<List<WeatherStation>> fetchAllWeatherStations() {
        final List<WeatherStation> cachedValue = weatherStationsCache.getCachedValue();

        if (cachedValue != null) {
            log.info("Returning cached weather stations");
            return Mono.just(cachedValue);
        }

        return weatherStationsDataFetcher.getWeatherStations()
                .doOnNext(weatherStationsCache::cacheValue);
    }

}
