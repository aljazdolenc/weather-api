package com.example.weatherapi.api;

import com.example.weatherapi.exceptions.validation.InvalidLatitudeException;
import com.example.weatherapi.exceptions.validation.InvalidLongitudeException;
import com.example.weatherapi.generated.api.CurrentApiDelegate;
import com.example.weatherapi.generated.model.GetCurrent200Response;
import com.example.weatherapi.service.WeatherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class WeatherApiHandler implements CurrentApiDelegate {
    private static final String UNEXPECTED_ERROR_MESSAGE = "Unexpected error occurred: ";
    private final WeatherService weatherService;

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return CurrentApiDelegate.super.getRequest();
    }

    @Override
    public ResponseEntity<GetCurrent200Response> getCurrent(BigDecimal lat, BigDecimal lon) {
        try {
            log.info("Received request for lat: {}, lon: {}", lat, lon);

            validateGeoCoordinates(lat, lon);

            return weatherService.getCurrent(lat, lon)
                    .doOnNext(stationsList -> log.info("Returning {} stations", stationsList.size()))
                    .map(stationsList -> new ResponseEntity<>(
                            new GetCurrent200Response().stations(stationsList),
                            HttpStatus.OK
                    ))
                    .block();
        } catch (InvalidLatitudeException | InvalidLongitudeException exception) {
            log.error("Invalid request: " + exception.getMessage(), exception);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage(), exception);
        } catch (Exception exception) {
            log.error(UNEXPECTED_ERROR_MESSAGE, exception);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, exception.getMessage(), exception);
        }
    }

    private void validateGeoCoordinates(final BigDecimal latitude, final BigDecimal longitude) {
        if (latitude != null && (latitude.compareTo(BigDecimal.valueOf(90)) > 0 || latitude.compareTo(BigDecimal.valueOf(-90)) < 0)) {
            throw new InvalidLatitudeException("Latitude must be between -90 and 90, got: " + latitude);
        }

        if (longitude != null && (longitude.compareTo(BigDecimal.valueOf(180)) > 0 || longitude.compareTo(BigDecimal.valueOf(-180)) < 0)) {
            throw new InvalidLatitudeException("Longitude must be between -180 and 180, got: " + longitude);
        }
    }

}
