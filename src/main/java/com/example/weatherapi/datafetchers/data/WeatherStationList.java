package com.example.weatherapi.datafetchers.data;

import com.example.weatherapi.datafetchers.data.WeatherStation;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.List;

@XmlRootElement(name = "data")
public class WeatherStationList {

    @XmlElement(name = "metData")
    private List<WeatherStation> weatherStations;

    public List<WeatherStation> getWeatherStations() {
        return this.weatherStations;
    }

}
