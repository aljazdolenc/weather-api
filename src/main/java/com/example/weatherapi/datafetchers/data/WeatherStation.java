package com.example.weatherapi.datafetchers.data;

import com.example.weatherapi.generated.model.WeatherStationInfo;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.math.BigDecimal;

@XmlRootElement(name = "metData")
@XmlAccessorType(XmlAccessType.FIELD)
public class WeatherStation {

    @XmlElement(name = "domain_lat")
    private BigDecimal latitude;

    @XmlElement(name = "domain_lon")
    private BigDecimal longitude;

    @XmlElement(name = "domain_title")
    private String name;

    @XmlElement(name = "t")
    private BigDecimal temperature;

    @XmlElement(name = "rh")
    private BigDecimal humidity;

    public BigDecimal getLat() {
        return latitude;
    }

    public BigDecimal getLon() {
        return longitude;
    }

    public WeatherStationInfo toWeatherStationInfo(final Double distanceFromTarget) {
        final WeatherStationInfo weatherStationInfo = new WeatherStationInfo();

        weatherStationInfo.setLat(latitude);
        weatherStationInfo.setLon(longitude);
        weatherStationInfo.setName(name);
        weatherStationInfo.setTemperature(temperature);
        weatherStationInfo.setHumidity(humidity);
        weatherStationInfo.setDistance(distanceFromTarget != null ? BigDecimal.valueOf(distanceFromTarget) : null);

        return weatherStationInfo;
    }

}
