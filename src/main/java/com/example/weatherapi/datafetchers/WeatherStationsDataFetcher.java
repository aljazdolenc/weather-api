package com.example.weatherapi.datafetchers;

import com.example.weatherapi.datafetchers.data.WeatherStation;
import com.example.weatherapi.datafetchers.data.WeatherStationList;
import com.example.weatherapi.service.EnvironmentService;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.StringReader;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class WeatherStationsDataFetcher {

    private final EnvironmentService environmentService;

    public Mono<List<WeatherStation>> getWeatherStations() {
        log.info("Fetching weather stations from MeteoArso: " + environmentService.arsoUrl);

        return WebClient.create().get()
                .uri(environmentService.arsoUrl)
                .retrieve()
                .onRawStatus(
                        status -> status != 200,
                        response -> Mono.error(new RuntimeException("MeteoArso request failed, status code: " + response.rawStatusCode()))
                )
                .bodyToMono(String.class)
                .elapsed()
                .doOnNext(tuple -> log.info("MeteoArso request took {} ms", tuple.getT1()))
                .map(tuple -> parseXmlToWeatherStations(tuple.getT2()))
                .doOnError(error -> {
                    log.error("MeteoArso request failed, reason \"{}\"", error.getMessage());
                    throw new RuntimeException("MeteoArso request failed, reason: " + error.getMessage(), error);
                });
    }

    private List<WeatherStation> parseXmlToWeatherStations(String xmlString) {
        try {
            final JAXBContext context = JAXBContext.newInstance(WeatherStationList.class);
            final Unmarshaller jaxbUnmarshaller = context.createUnmarshaller();
            final StringReader reader = new StringReader(xmlString);
            final WeatherStationList station = (WeatherStationList) jaxbUnmarshaller.unmarshal(reader);
            final List<WeatherStation> weatherStations = station.getWeatherStations();

            if (weatherStations == null || weatherStations.isEmpty()) {
                log.error("No weather stations found in MeteoArso response");
                throw new RuntimeException("No weather stations found in MeteoArso response");
            }

            return weatherStations;
        } catch (JAXBException e) {
            log.error("Error while parsing MeteoArso response: {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

}
