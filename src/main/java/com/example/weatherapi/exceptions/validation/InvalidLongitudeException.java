package com.example.weatherapi.exceptions.validation;

public class InvalidLongitudeException extends RuntimeException {

        public InvalidLongitudeException(final String message) {
            super(message);
        }

}
