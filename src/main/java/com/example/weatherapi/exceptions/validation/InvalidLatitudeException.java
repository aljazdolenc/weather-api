package com.example.weatherapi.exceptions.validation;

public class InvalidLatitudeException extends RuntimeException {

    public InvalidLatitudeException(final String message) {
        super(message);
    }

}
