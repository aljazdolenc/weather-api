package com.example.weatherapi;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class WeatherApiApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(WeatherApiApplication.class).run(args);
    }

}
